"""."""
from apps import app
from flask import (render_template, request, flash, session,
                   url_for, redirect, jsonify)
from werkzeug.utils import secure_filename
from apps import login_manager
from flask_login import login_required, login_user, logout_user
import json
import os
import string
import random
# import yaml

from .forms import SigninForm, RegistrationForm
from .models import db, User, Project, ApiRequests, TestCases
from .config import ALLOWED_EXTENSIONS
from .utils import uploadFile, nocache


@login_manager.unauthorized_handler
def unauthorized_callback():
    """."""
    return redirect('/login?next=' + request.path)


@login_manager.user_loader
def load_user(user_id):
    """."""
    user = User.query.get(int(user_id))
    session['user_id'] = user_id
    return user

##############################


@app.route('/login', methods=['GET'])
def login():
    """."""
    signin_form = SigninForm()
    return render_template('v2/login_1.html',
                           form=signin_form)


@app.route('/')
@login_required
def home():
    """."""
    return render_template('index.html')
