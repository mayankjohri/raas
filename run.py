"""
Maya REST API Automation Solution.

Main file to start the RAAS server.
"""
# run.py

from apps import app

if __name__ == "__main__":
    app.run(host='localhost', port=8081, debug=True)
 
